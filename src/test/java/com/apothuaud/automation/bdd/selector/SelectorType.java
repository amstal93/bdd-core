package com.apothuaud.automation.bdd.selector;

public enum SelectorType {
    id, xpath
}
