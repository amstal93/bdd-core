$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/api/jsonplaceholder/create-album.feature");
formatter.feature({
  "name": "[API][Jsonplaceholder] Create albums",
  "description": "  As a User of Jsonplaceholder API\n  I should be able to create albums",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    }
  ]
});
formatter.scenario({
  "name": "Nominal case - v1",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url template \"${PROTOCOL}://${DOMAIN}${ENDPOINT}\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrlTemplate(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url protocol \"https\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseProtocol(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url domain \"jsonplaceholder.typicode.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseDomain(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url endpoint \"/albums\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseEndpoint(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request header \"Content-Type\" as \"application/x-www-form-urlencoded\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseHeaderAs(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request form param \"userId\" as \"1\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseFormParamAs(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request form param \"title\" as \"My album title\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseFormParamAs(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request method \"POST\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestMethod(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send the request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendTheRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attribute \"userId\" equals to \"1\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributeEqualsTo(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attribute \"title\" equals to \"My album title\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributeEqualsTo(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attribute \"id\" present",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributePresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Nominal case - v2",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://jsonplaceholder.typicode.com/albums\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request header \"Content-Type\" as \"application/x-www-form-urlencoded\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseHeaderAs(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request form params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "userId",
        "1"
      ]
    },
    {
      "cells": [
        "title",
        "My album title"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestFormParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"POST\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "userId",
        "1"
      ]
    },
    {
      "cells": [
        "title",
        "My album title"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Nominal case - v3",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://jsonplaceholder.typicode.com/albums\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/jsonplaceholder/request/POST-create-album.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"POST\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have json response with reference from file \"samples/api/jsonplaceholder/response/POST-create-album.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonWithReferenceFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/jsonplaceholder/delete-album.feature");
formatter.feature({
  "name": "[API][Jsonplaceholder] Delete albums",
  "description": "  As a User of Jsonplaceholder API\n  I should be able to delete albums",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    }
  ]
});
formatter.scenario({
  "name": "Nominal case - v1",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url template \"${PROTOCOL}://${DOMAIN}${ENDPOINT}\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrlTemplate(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url protocol \"https\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseProtocol(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url domain \"jsonplaceholder.typicode.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseDomain(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url endpoint \"/albums/1\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseEndpoint(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request method \"DELETE\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestMethod(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send the request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendTheRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Nominal case - v2",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://jsonplaceholder.typicode.com/albums/5\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"DELETE\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/jsonplaceholder/read-album.feature");
formatter.feature({
  "name": "[API][Jsonplaceholder] Read album details",
  "description": "  As a User of Jsonplaceholder API\n  I should be able to read album details",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    }
  ]
});
formatter.scenario({
  "name": "Nominal case - v1",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url template \"${PROTOCOL}://${DOMAIN}${ENDPOINT}\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrlTemplate(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url protocol \"https\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseProtocol(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url domain \"jsonplaceholder.typicode.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseDomain(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url endpoint \"/albums/1\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseEndpoint(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request method \"GET\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestMethod(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send the request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendTheRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attribute \"id\" present",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributePresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attribute \"userId\" present",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributePresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attribute \"title\" present",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributePresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Read an album - v2",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://jsonplaceholder.typicode.com/albums/14567890987654\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"404\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/jsonplaceholder/read-albums.feature");
formatter.feature({
  "name": "[API][Jsonplaceholder] Read Albums List",
  "description": "  As a User of Jsonplaceholder API\n  I should be able to read albums list",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    }
  ]
});
formatter.scenario({
  "name": "Nominal case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://jsonplaceholder.typicode.com/albums\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have json response with reference from file \"samples/api/jsonplaceholder/response/GET-albums.json\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonWithReferenceFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/jsonplaceholder/update-album.feature");
formatter.feature({
  "name": "[API][Jsonplaceholder] Update an Album",
  "description": "  As a User of Jsonplaceholder API\n  I should be able to update an album",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    }
  ]
});
formatter.scenario({
  "name": "Nominal case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Jsonplaceholder"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://jsonplaceholder.typicode.com/albums/1\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/jsonplaceholder/request/PUT-update-album.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"PUT\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have json response with reference from file \"samples/api/jsonplaceholder/response/PUT-update-album.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonWithReferenceFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/spotify/v1/create_a_playlist.feature");
formatter.feature({
  "name": "[API][Spotify] Create a Playlist",
  "description": "  As a user of Spotify Api\n  I should be able to create Playlists",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://accounts.spotify.com/authorize\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "client_id",
        "14ad74980cc24ecb9aff88ded35986e8"
      ]
    },
    {
      "cells": [
        "response_type",
        "token"
      ]
    },
    {
      "cells": [
        "redirect_uri",
        "http://www.example.com/postman/redirect"
      ]
    },
    {
      "cells": [
        "state",
        "123"
      ]
    },
    {
      "cells": [
        "scope",
        "playlist-modify-private"
      ]
    },
    {
      "cells": [
        "show_dialog",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I don\u0027t want to follow redirects",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iDonTWantToFollowRedirects()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with header \"Location\" present",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseHeaderPresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save response header \"Location\" as scenario param \"authenticationRedirect\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveResponseHeaderAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"${authenticationRedirect}\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-username\" with text \"esensqatraining@yopmail.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-password\" with text \"Passwd1!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003elogin-button\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#access_token\u003d\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save web client url in scenario param \"urlAfterAuthentication\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iSaveDriverUrlInScenarioParamsAs(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I extract regexp \"_token\u003d(.*)\u0026token_\" from \"urlAfterAuthentication\" to \"authenticationToken\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsActions.iExtractRegexpFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/users/2g5o4vq6ayxp9eo5xlqw6os2k/playlists\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/POST_create_playlist.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"POST\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes present",
  "rows": [
    {
      "cells": [
        "jsonPath"
      ]
    },
    {
      "cells": [
        "external_urls"
      ]
    },
    {
      "cells": [
        "followers"
      ]
    },
    {
      "cells": [
        "href"
      ]
    },
    {
      "cells": [
        "images"
      ]
    },
    {
      "cells": [
        "owner"
      ]
    },
    {
      "cells": [
        "tracks"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesPresent(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "name",
        "My Training Playlist"
      ]
    },
    {
      "cells": [
        "description",
        "Playlist for automation tests"
      ]
    },
    {
      "cells": [
        "public",
        "true"
      ]
    },
    {
      "cells": [
        "collaborative",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Error Case - no auth token",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/users/2g5o4vq6ayxp9eo5xlqw6os2k/playlists\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/POST_create_playlist.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"POST\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"401\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "error.message",
        "No token provided"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/spotify/v1/playlist_scenario.feature");
formatter.feature({
  "name": "[API][Spotify] CRUD on Playlist",
  "description": "  As a user of Spotify Api\n  I should be able to CRUD Playlists",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@Nominal"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://accounts.spotify.com/authorize\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "client_id",
        "14ad74980cc24ecb9aff88ded35986e8"
      ]
    },
    {
      "cells": [
        "response_type",
        "token"
      ]
    },
    {
      "cells": [
        "redirect_uri",
        "http://www.example.com/postman/redirect"
      ]
    },
    {
      "cells": [
        "state",
        "123"
      ]
    },
    {
      "cells": [
        "scope",
        "playlist-modify-private"
      ]
    },
    {
      "cells": [
        "show_dialog",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I don\u0027t want to follow redirects",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iDonTWantToFollowRedirects()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with header \"Location\" present",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseHeaderPresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save response header \"Location\" as scenario param \"authenticationRedirect\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveResponseHeaderAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"${authenticationRedirect}\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-username\" with text \"esensqatraining@yopmail.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-password\" with text \"Passwd1!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003elogin-button\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#access_token\u003d\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save web client url in scenario param \"urlAfterAuthentication\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iSaveDriverUrlInScenarioParamsAs(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I extract regexp \"_token\u003d(.*)\u0026token_\" from \"urlAfterAuthentication\" to \"authenticationToken\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsActions.iExtractRegexpFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/me\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save response json attribute \"id\" as scenario param \"userId\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveResponseJsonAttributeAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/me/playlists\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "limit",
        "50"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save size of response json array \"items\" as scenario param \"nbPlaylistsBefore\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveSizeOfResponseJsonArrayAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/users/${userId}/playlists\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/POST_create_playlist.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"POST\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save response json attribute \"id\" as scenario param \"newPlaylistId\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveResponseJsonAttributeAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/me/playlists\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "limit",
        "50"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save size of response json array \"items\" as scenario param \"nbPlaylistsAfter\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveSizeOfResponseJsonArrayAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have scenario param \"nbPlaylistsAfter\" greater than scenario param \"nbPlaylistsBefore\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsValidations.iShouldHaveScenarioParamGreaterThanScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/${newPlaylistId}\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save size of response json array \"tracks.items\" as scenario param \"nbTracksBefore\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveSizeOfResponseJsonArrayAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/${newPlaylistId}/tracks\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/POST_add_tracks.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"POST\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/${newPlaylistId}/tracks\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save size of response json array \"items\" as scenario param \"nbTracksAfter\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveSizeOfResponseJsonArrayAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have scenario param \"nbTracksAfter\" greater than scenario param \"nbTracksBefore\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsValidations.iShouldHaveScenarioParamGreaterThanScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/${newPlaylistId}\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/PUT_update_playlist.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"PUT\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/${newPlaylistId}\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "name",
        "My Training Playlist"
      ]
    },
    {
      "cells": [
        "description",
        "Updated one more time !"
      ]
    },
    {
      "cells": [
        "public",
        "true"
      ]
    },
    {
      "cells": [
        "collaborative",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/${newPlaylistId}/followers\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"DELETE\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/me/playlists\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "limit",
        "50"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save size of response json array \"items\" as scenario param \"nbPlaylistsAfter\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveSizeOfResponseJsonArrayAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have scenario param \"nbPlaylistsAfter\" equals to scenario param \"nbPlaylistsBefore\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsValidations.iShouldHaveScenarioParamEqualsToScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/spotify/v1/read_a_playlist.feature");
formatter.feature({
  "name": "[API][Spotify] Read a Playlist",
  "description": "  As a user of Spotify Api\n  I should be able to read Playlist details",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://accounts.spotify.com/authorize\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "client_id",
        "14ad74980cc24ecb9aff88ded35986e8"
      ]
    },
    {
      "cells": [
        "response_type",
        "token"
      ]
    },
    {
      "cells": [
        "redirect_uri",
        "http://www.example.com/postman/redirect"
      ]
    },
    {
      "cells": [
        "state",
        "123"
      ]
    },
    {
      "cells": [
        "scope",
        "playlist-modify-private"
      ]
    },
    {
      "cells": [
        "show_dialog",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I don\u0027t want to follow redirects",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iDonTWantToFollowRedirects()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with header \"Location\" present",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseHeaderPresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save response header \"Location\" as scenario param \"authenticationRedirect\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveResponseHeaderAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"${authenticationRedirect}\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-username\" with text \"esensqatraining@yopmail.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-password\" with text \"Passwd1!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003elogin-button\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#access_token\u003d\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save web client url in scenario param \"urlAfterAuthentication\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iSaveDriverUrlInScenarioParamsAs(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I extract regexp \"_token\u003d(.*)\u0026token_\" from \"urlAfterAuthentication\" to \"authenticationToken\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsActions.iExtractRegexpFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/1cZirM1Yv7bcx9nj2ZDNOo\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "name",
        "[DoNotTouch] My Persistent Playlist"
      ]
    },
    {
      "cells": [
        "description",
        "Playlist for automation tests. Do not delete or modify !"
      ]
    },
    {
      "cells": [
        "public",
        "false"
      ]
    },
    {
      "cells": [
        "collaborative",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Error Case - no auth token",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/1cZirM1Yv7bcx9nj2ZDNOo\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"401\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "error.message",
        "No token provided"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/api/spotify/v1/update_a_playlist.feature");
formatter.feature({
  "name": "[API][Spotify] Update a Playlist",
  "description": "  As a user of Spotify Api\n  I should be able to update Playlist details",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://accounts.spotify.com/authorize\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request query params as",
  "rows": [
    {
      "cells": [
        "paramName",
        "paramValue"
      ]
    },
    {
      "cells": [
        "client_id",
        "14ad74980cc24ecb9aff88ded35986e8"
      ]
    },
    {
      "cells": [
        "response_type",
        "token"
      ]
    },
    {
      "cells": [
        "redirect_uri",
        "http://www.example.com/postman/redirect"
      ]
    },
    {
      "cells": [
        "state",
        "123"
      ]
    },
    {
      "cells": [
        "scope",
        "playlist-modify-public"
      ]
    },
    {
      "cells": [
        "show_dialog",
        "false"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestQueryParamsAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I don\u0027t want to follow redirects",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iDonTWantToFollowRedirects()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"GET\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with header \"Location\" present",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseHeaderPresent(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save response header \"Location\" as scenario param \"authenticationRedirect\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiValidations.iSaveResponseHeaderAsScenarioParam(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"${authenticationRedirect}\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-username\" with text \"esensqatraining@yopmail.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003elogin-password\" with text \"Passwd1!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003elogin-button\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#access_token\u003d\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I save web client url in scenario param \"urlAfterAuthentication\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iSaveDriverUrlInScenarioParamsAs(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I extract regexp \"_token\u003d(.*)\u0026token_\" from \"urlAfterAuthentication\" to \"authenticationToken\"",
  "keyword": "And "
});
formatter.match({
  "location": "ScenarioParamsActions.iExtractRegexpFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/4g7UzQ4im7lbuEJhyEgJWJ\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Authorization",
        "Bearer ${authenticationToken}"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/PUT_update_playlist.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"PUT\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with status code \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseWithStatusCode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Error Case - no auth token",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@API"
    },
    {
      "name": "@Spotify"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://api.spotify.com/v1/playlists/4g7UzQ4im7lbuEJhyEgJWJ\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request headers as",
  "rows": [
    {
      "cells": [
        "headerName",
        "headerValue"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestHeadersAs(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use request json body from file \"samples/api/spotify/v1/request/PUT_update_playlist.json\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApiSettings.iWantToUseRequestBodyFromFile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send \"PUT\" request",
  "keyword": "When "
});
formatter.match({
  "location": "ApiActions.iSendRequest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should have response with json attributes equals to",
  "rows": [
    {
      "cells": [
        "jsonPath",
        "expected"
      ]
    },
    {
      "cells": [
        "error.message",
        "No token provided"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "ApiValidations.iShouldHaveResponseJsonAttributesEqualsTo(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/mobile/android/calculator/addition.feature");
formatter.feature({
  "name": "[ANDROID][Calculator] Additions",
  "description": "  As a user of Android Calculator app\n  I should be able to perform additions",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@MOBILE"
    },
    {
      "name": "@ANDROID"
    },
    {
      "name": "@Calculator"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Nominal cases",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.step({
  "name": "I want to use appium hub url \"http://localhost:4723/wd/hub\"",
  "keyword": "Given "
});
formatter.step({
  "name": "I want to use appium platform name \"Android\"",
  "keyword": "And "
});
formatter.step({
  "name": "I want to use appium device name \"Nexus_4_API_27\"",
  "keyword": "And "
});
formatter.step({
  "name": "I want to use appium app \"samples/mobile/android/calculator/apk/app-debug.apk\"",
  "keyword": "And "
});
formatter.step({
  "name": "I open android client",
  "keyword": "When "
});
formatter.step({
  "name": "I should see android element \"id-\u003ecom.example.eyakub.calculator:id/textView\"",
  "keyword": "Then "
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttonDel\"",
  "keyword": "When "
});
formatter.step({
  "name": "I click on android element \"\u003cnumber1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I click on android element \"\u003coperator\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I click on android element \"\u003cnumber2\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttoneql\"",
  "keyword": "And "
});
formatter.step({
  "name": "I should see text \"\u003cresult\u003e\" in android element \"id-\u003ecom.example.eyakub.calculator:id/textView\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "number1",
        "operator",
        "number2",
        "result"
      ]
    },
    {
      "cells": [
        "id-\u003ecom.example.eyakub.calculator:id/button1",
        "id-\u003ecom.example.eyakub.calculator:id/buttonadd",
        "id-\u003ecom.example.eyakub.calculator:id/button2",
        "3.0"
      ]
    },
    {
      "cells": [
        "id-\u003ecom.example.eyakub.calculator:id/button3",
        "id-\u003ecom.example.eyakub.calculator:id/buttonsub",
        "id-\u003ecom.example.eyakub.calculator:id/button1",
        "2.0"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Nominal cases",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MOBILE"
    },
    {
      "name": "@ANDROID"
    },
    {
      "name": "@Calculator"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium hub url \"http://localhost:4723/wd/hub\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumHubUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium platform name \"Android\"",
  "keyword": "And "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumPlatformName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium device name \"Nexus_4_API_27\"",
  "keyword": "And "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumDeviceName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium app \"samples/mobile/android/calculator/apk/app-debug.apk\"",
  "keyword": "And "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumApp(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open android client",
  "keyword": "When "
});
formatter.match({
  "location": "AndroidActions.iOpenAndroidClient()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see android element \"id-\u003ecom.example.eyakub.calculator:id/textView\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AndroidValidations.iShouldSeeAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttonDel\"",
  "keyword": "When "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/button1\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttonadd\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/button2\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttoneql\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see text \"3.0\" in android element \"id-\u003ecom.example.eyakub.calculator:id/textView\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AndroidValidations.iShouldSeeTextInAndroidElement(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Nominal cases",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MOBILE"
    },
    {
      "name": "@ANDROID"
    },
    {
      "name": "@Calculator"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium hub url \"http://localhost:4723/wd/hub\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumHubUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium platform name \"Android\"",
  "keyword": "And "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumPlatformName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium device name \"Nexus_4_API_27\"",
  "keyword": "And "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumDeviceName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use appium app \"samples/mobile/android/calculator/apk/app-debug.apk\"",
  "keyword": "And "
});
formatter.match({
  "location": "MobileSettings.iWantToUseAppiumApp(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open android client",
  "keyword": "When "
});
formatter.match({
  "location": "AndroidActions.iOpenAndroidClient()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see android element \"id-\u003ecom.example.eyakub.calculator:id/textView\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AndroidValidations.iShouldSeeAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttonDel\"",
  "keyword": "When "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/button3\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttonsub\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/button1\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on android element \"id-\u003ecom.example.eyakub.calculator:id/buttoneql\"",
  "keyword": "And "
});
formatter.match({
  "location": "AndroidActions.iClickOnAndroidElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see text \"2.0\" in android element \"id-\u003ecom.example.eyakub.calculator:id/textView\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AndroidValidations.iShouldSeeTextInAndroidElement(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/web/cura-healthcare/appointment.feature");
formatter.feature({
  "name": "[WEB][CuraHealthcare] Appointment",
  "description": "  As a user of Cura Healthcare web app\n  I should be able to make appointments",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@CuraHealthcare"
    }
  ]
});
formatter.scenario({
  "name": "Unauthentified appointment",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@CuraHealthcare"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://katalon-demo-cura.herokuapp.com/\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003ebtn-make-appointment\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ebtn-make-appointment\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003etxt-username\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003etxt-username\" with text \"John Doe\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003etxt-password\" with text \"ThisIsNotAPassword\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ebtn-login\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#appointment\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003ebtn-book-appointment\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ebtn-book-appointment\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"xpath-\u003e//div[contains(@class, \u0027datepicker\u0027)]\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"xpath-\u003e/html/body/div/div[1]/table/thead/tr[2]/th[3]\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"xpath-\u003e/html/body/div/div[1]/table/tbody/tr[1]/td[3]\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ebtn-book-appointment\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#summary\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003efacility\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to url \"https://katalon-demo-cura.herokuapp.com/history.php#history\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iNavigateToUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003ehistory\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/web/cura-healthcare/authentication.feature");
formatter.feature({
  "name": "[WEB][CuraHealthcare] Authentication",
  "description": "  As a user of Cura Healthcare web app\n  I should be able to authenticate",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@CuraHealthcare"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@CuraHealthcare"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://katalon-demo-cura.herokuapp.com/profile.php#login\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003etxt-username\" with text \"John Doe\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003etxt-password\" with text \"ThisIsNotAPassword\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ebtn-login\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003e#appointment\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003ecombo_facility\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Error Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@CuraHealthcare"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://katalon-demo-cura.herokuapp.com/profile.php#login\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003etxt-username\" with text \"azertyuiop\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003etxt-password\" with text \"azertyuiop\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ebtn-login\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"xpath-\u003e//p[contains(@class, \u0027text-danger\u0027)]\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see text \"Login failed!\" in web element \"xpath-\u003e//p[contains(@class, \u0027text-danger\u0027)]\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeTextInElement(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/web/the-internet/checkboxes.feature");
formatter.feature({
  "name": "[WEB][TheInternet] Checkboxes",
  "description": "  As a user of bdd core framework\n  I should be able to check and validate checkboxes",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@TheInternet"
    }
  ]
});
formatter.scenario({
  "name": "Play with checkboxes",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@TheInternet"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://the-internet.herokuapp.com/checkboxes\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"xpath-\u003e//*[@id\u003d\u0027checkboxes\u0027]/input[1]\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"xpath-\u003e//*[@id\u003d\u0027checkboxes\u0027]/input[1]\" selected",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeWebElementSelected(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"xpath-\u003e//*[@id\u003d\u0027checkboxes\u0027]/input[2]\" selected",
  "keyword": "And "
});
formatter.match({
  "location": "WebValidations.iShouldSeeWebElementSelected(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/web/the-internet/form-authentication.feature");
formatter.feature({
  "name": "[WEB][TheInternet] Form Authentication",
  "description": "  As a user of TheInternet web app\n  I should be able to authenticate using form",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@TheInternet"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@TheInternet"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://the-internet.herokuapp.com/login\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003eusername\" with text \"tomsmith\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003epassword\" with text \"SuperSecretPassword!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"xpath-\u003e//button//i[contains(text(), \u0027 Login\u0027)]\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I wait for web condition \"urlContains-\u003esecure\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iWaitForWebCondition(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"xpath-\u003e//a//i[contains(text(), \u0027 Logout\u0027)]\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Error Case - Invalid username",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@TheInternet"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://the-internet.herokuapp.com/login\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003eusername\" with text \"nottomsmith\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003epassword\" with text \"SuperSecretPassword!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"xpath-\u003e//button//i[contains(text(), \u0027 Login\u0027)]\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003eflash\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see text \"Your username is invalid!\" in web element \"id-\u003eflash\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebValidations.iShouldSeeTextInElement(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Error Case - Invalid password",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@TheInternet"
    },
    {
      "name": "@ErrorCase"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "Given "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://the-internet.herokuapp.com/login\"",
  "keyword": "And "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003eusername\" with text \"tomsmith\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003epassword\" with text \"NotSuperSecretPassword!\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"xpath-\u003e//button//i[contains(text(), \u0027 Login\u0027)]\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003eflash\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see text \"Your password is invalid!\" in web element \"id-\u003eflash\"",
  "keyword": "And "
});
formatter.match({
  "location": "WebValidations.iShouldSeeTextInElement(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/web/yahoo/create_account.feature");
formatter.feature({
  "name": "[WEB][Yahoo] Create Account",
  "description": "  As a user of Yahoo web app\n  I should be able to create an account",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@Yahoo"
    }
  ]
});
formatter.scenario({
  "name": "Nominal Case",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB"
    },
    {
      "name": "@Yahoo"
    },
    {
      "name": "@Nominal"
    },
    {
      "name": "@Critical"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I want to use url \"https://login.yahoo.com/\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UrlSettings.iWantToUseUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I want to use \"Chrome\" web driver",
  "keyword": "And "
});
formatter.match({
  "location": "WebSettings.iWantToUseDriver(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open web client",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iOpenWebDriver()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003ecreateacc\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on web element \"id-\u003ecreateacc\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iClickOnElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see web element \"id-\u003eusernamereg-firstName\"",
  "keyword": "Then "
});
formatter.match({
  "location": "WebValidations.iShouldSeeElement(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I fill web field \"id-\u003eusernamereg-firstName\" with text \"Esens\"",
  "keyword": "When "
});
formatter.match({
  "location": "WebActions.iFillFieldWith(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});